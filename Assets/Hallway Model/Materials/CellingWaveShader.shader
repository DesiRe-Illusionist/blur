﻿Shader "Custom/Waves"
{
    Properties
    {
        _Color("Color", Color) = (0, 0, 0, 1)
        _Strength("Strength", Range(0,2)) = 1.0
        _Speed("Speed", Range(-200, 200)) = 100
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        Pass
        {

            Tags {"LightMode"="ForwardBase"}

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc"

            float4 _Color;
            float _Strength;
            float _Speed;

            struct vertexOutput {
                float2 uv : TEXCOORD0;
                float4 pos : SV_POSITION;
                fixed4 diff : COLOR0;
            };

            vertexOutput vert(appdata_base v) {

                vertexOutput o;

                float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
                float displacement = (sin(worldPos.x + (_Speed * _Time)));
                worldPos.y = worldPos.y + (displacement * _Strength);

                o.pos = mul(UNITY_MATRIX_VP, worldPos);

                // o.vertex = UnityObjectToClipPos(IN.vertex);
                o.uv = v.texcoord;

                half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                o.diff = nl * _LightColor0;

                o.diff.rgb += ShadeSH9(half4(worldNormal,1));

                return o;
            }

            float4 frag(vertexOutput IN) : COLOR {
                return _Color;
            }
            ENDCG
        }
    }
}