﻿Shader "Custom/Waves"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Strength("Strength", Range(0,2)) = 1.0
        _Speed("Speed", Range(-200, 200)) = 100
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        Pass
        {

            Tags {"LightMode"="ForwardBase"}

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc"

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Strength;
            float _Speed;

            struct vertexOutput {
                float2 uv : TEXCOORD0;
                float4 pos : SV_POSITION;
                fixed4 diff : COLOR0;
            };

            vertexOutput vert(appdata_base v) {

                vertexOutput o;

                float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
                float displacement = (sin(worldPos.x + (_Speed * _Time)));
                worldPos.y = worldPos.y + (displacement * _Strength);

                o.pos = mul(UNITY_MATRIX_VP, worldPos);

                // o.vertex = UnityObjectToClipPos(IN.vertex);
                o.uv = v.texcoord;

                half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                o.diff = nl * _LightColor0;

                o.diff.rgb += ShadeSH9(half4(worldNormal,1));

                return o;
            }

            fixed4 frag(vertexOutput IN) : SV_Target {
                fixed4 col = tex2D(_MainTex, IN.uv);
                col *= IN.diff;
                return col;
            }
            ENDCG
        }
    }
}